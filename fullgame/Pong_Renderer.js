/**
* Author: Iker Jamardo Zugaza
* Company: Ludei (www.ludei.com)
* Disclaimer: 
    Please, note that the purpose of this software is merely educational. Plenty
    of error checking should be added to make it for professional use.
* License: LGPL v3
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
* =====================
* Pong.Renderer
* =====================
* Represents the "abstraction" of a very simple renderer (tuned for the purpose of this game only).
*/
Pong.Renderer = function() {
};

Pong.Renderer.prototype = {
    /**
    * Initializes the renderer.
    */
    initialize : function() {
    },

    /**
    * Should be called when the rendering of a frame begins.
    */
    beginFrame : function() {
    },

    /**
    * Should be called when the rendering of a frame ends.
    */
    endFrame : function() {
    },

    /**
    * Renders a rectangle.
    * @param [number] x The horizontal initial position of the rectangle to be drawn.
    * @param [number] y The vertical initial position of the rectangle to be drawn.
    * @param [number] width The horizontal size of the rectangle to be drawn.
    * @param [number] height The vertical size of the rectangle to be drawn.
    * @param [string] color The color to of the rectangle to be drawn.
    */
    renderRectangle : function(x, y, width, height, color) {
    },

    /**
    * Renders a text.
    * @param [number] x The horizontal initial position of the text to be drawn.
    * @param [number] y The vertical initial position of the text to be drawn.
    * @param [string] text The text to be drawn.
    * @param [number] fontSize The size of the font to be used to draw the text.
    */
    renderText : function(x, y, text, fontSize) {
    },

    /**
    * Returns the horizontal size of the rendering area.
    */
    getWidth : function() {
    },

    /**
    * Returns the vertical size of the rendering area.
    */
    getHeight : function() {
    },

    /**
    * Clears the rendering area with a given color.
    * @param [string] color The color to clear the rendering area with.
    */
    clear : function(color) {
    }
};

/**
* =====================
* Pong.SpriteFont
* =====================
* Represents a helper structure that allows to draw text using sprites. It is initialized passing an image with all the
* characters as images in it. All of these images per character MUST be of the same size.
*/
Pong.SpriteFont = function() {
};

Pong.SpriteFont.prototype = {
    image : null,
    charRectangles : null,
    charWidth : 0,

    /**
    * Pass an image that contains all the characters horizontally distributed. All the characters MUSt be of the same size.
    * @param {Image} image The image object that will hold all the characters as images inside. All the characters MUST be of the same size and be 
    * distributed horizontally.
    * @param {string} chars A string that contains the characters that are represented in the same sequence inside the image. Examples: "abcde" or "012345".
    */
    initialize : function(image, chars) {
        this.image = image;
        this.charRectangles = {};

        var charsArray = chars.split("");

        this.charWidth = image.width / charsArray.length >> 0;

        for (var i = 0, x = 0; i < charsArray.length; i++, x += this.charWidth) {
            var charRectangle = new Pong.Rectangle();
            charRectangle.initialize(x, 0, this.charWidth, image.height); 
            this.charRectangles[charsArray[i]] = charRectangle;
        }
    },

    renderText : function(canvasContext2D, text, x, y, fontSize) {
        var scaleFactor = fontSize / this.image.height;
        var textCharsArray = text.split("");
        // Calculate the new coordinates to make the text to be always centered.
        // TODO: an alignment parameter to align horizontally (center, riight, left) and vertically (center, bottom, top).
        x = x - ((textCharsArray.length * this.charWidth * scaleFactor) >> 1);
        y = y - ((this.image.height * scaleFactor) >> 1);
        for(var i = 0; i < textCharsArray.length; i++) {
            var charRectangle = this.charRectangles[textCharsArray[i]];
            if (charRectangle) {
                canvasContext2D.drawImage(
                    this.image, 
                    charRectangle.x, 
                    charRectangle.y, 
                    charRectangle.width, 
                    charRectangle.height, 
                    x, 
                    y, 
                    charRectangle.width * scaleFactor, 
                    charRectangle.height * scaleFactor);
                x += charRectangle.width * scaleFactor;
            }
        }
    }
};

/**
* =====================
* Pong.RendererCanvas2D
* =====================
*/
Pong.RendererCanvas2D = function() {
};

Pong.extend(Pong.RendererCanvas2D, Pong.Renderer);

Pong.RendererCanvas2D.prototype.canvas = null;
Pong.RendererCanvas2D.prototype.canvasContext2D = null;
Pong.RendererCanvas2D.prototype.spriteFontManager = null;

Pong.RendererCanvas2D.prototype.initialize = function(canvas) {
    if (!canvas) {
        this.canvas = document.createElement(navigator.isCocoonJS ? "screencanvas" : "canvas");
        this.canvas.width = window.innerWidth;
        this.canvas.height = window.innerHeight;
        document.body.appendChild(this.canvas);
    }
    else {
        this.canvas = canvas;
    }
    this.canvasContext2D = this.canvas.getContext("2d");

    var image = new Image();
    image.rendererCanvas2D = this;
    image.src = "resources/PongNumbersSpriteFont.png";
    image.addEventListener("load", function(event) {
        var rendererCanvas2D = event.target.rendererCanvas2D;
        rendererCanvas2D.spriteFontManager = new Pong.SpriteFont();
        rendererCanvas2D.spriteFontManager.initialize(event.target, "0123456789");
    });
};

Pong.RendererCanvas2D.prototype.renderRectangle = function(x, y, width, height, color) {
    this.canvasContext2D.fillStyle = color;
    this.canvasContext2D.fillRect(x, y, width, height);
};

Pong.RendererCanvas2D.prototype.renderText = function(x, y, text, fontSize) {
    if (this.spriteFontManager) {
        this.spriteFontManager.renderText(this.canvasContext2D, text, x, y, fontSize)
    }
};

Pong.RendererCanvas2D.prototype.getWidth = function() {
    return this.canvas.width;
};

Pong.RendererCanvas2D.prototype.getHeight = function() {
    return this.canvas.height;
};

Pong.RendererCanvas2D.prototype.clear = function(color) {
    this.canvasContext2D.fillStyle = color;
    this.canvasContext2D.fillRect(0, 0, this.canvas.width, this.canvas.height);
};

/**
* =====================
* Pong.RendererCanvasWebGL
* =====================
*/
Pong.RendererCanvasWebGL = function() {
};

Pong.extend(Pong.RendererCanvasWebGL, Pong.Renderer);

Pong.RendererCanvasWebGL.prototype.canvas = null;
Pong.RendererCanvasWebGL.prototype.gl = null;
Pong.RendererCanvasWebGL.prototype.shaderProgram = null;
Pong.RendererCanvasWebGL.prototype.cubeVertexPositionBuffer = null;
Pong.RendererCanvasWebGL.prototype.cubeVertexColorBuffer = null;
Pong.RendererCanvasWebGL.prototype.cubeVertexIndexBuffer = null;
Pong.RendererCanvasWebGL.prototype.mvMatrix = null;
Pong.RendererCanvasWebGL.prototype.mvMatrixStack = null;
Pong.RendererCanvasWebGL.prototype.cubeVertexIndexBuffer = null;

Pong.RendererCanvasWebGL.prototype.initCubeBuffers = function(id) {
    this.cubeVertexPositionBuffer = this.gl.createBuffer();
    this.gl.bindBuffer(this.gl.ARRAY_BUFFER, this.cubeVertexPositionBuffer);
    var vertices = [
        // Front face
        0.0, 0.0,  1.0,
         1.0, 0.0,  1.0,
         1.0,  1.0,  1.0,
        0.0,  1.0,  1.0,

        // Back face
        0.0, 0.0, 0.0,
        0.0,  1.0, 0.0,
         1.0,  1.0, 0.0,
         1.0, 0.0, 0.0,

        // Top face
        0.0,  1.0, 0.0,
        0.0,  1.0,  1.0,
         1.0,  1.0,  1.0,
         1.0,  1.0, 0.0,

        // Bottom face
        0.0, 0.0, 0.0,
         1.0, 0.0, 0.0,
         1.0, 0.0,  1.0,
        0.0, 0.0,  1.0,

        // Right face
         1.0, 0.0, 0.0,
         1.0,  1.0, 0.0,
         1.0,  1.0,  1.0,
         1.0, 0.0,  1.0,

        // Left face
        0.0, 0.0, 0.0,
        0.0, 0.0,  1.0,
        0.0,  1.0,  1.0,
        0.0,  1.0, 0.0
    ];
    this.gl.bufferData(this.gl.ARRAY_BUFFER, new Float32Array(vertices), this.gl.STATIC_DRAW);
    this.cubeVertexPositionBuffer.itemSize = 3;
    this.cubeVertexPositionBuffer.numItems = 24;

    this.cubeVertexColorBuffer = this.gl.createBuffer();
    this.gl.bindBuffer(this.gl.ARRAY_BUFFER, this.cubeVertexColorBuffer);
    var colors = [
        [1.0, 1.0, 1.0, 1.0], // Front face
        [0.3, 0.3, 0.3, 1.0], // Back face
        [0.6, 0.6, 0.6, 1.0], // Top face
        [0.6, 0.6, 0.6, 1.0], // Bottom face
        [0.6, 0.6, 0.6, 1.0], // Right face
        [0.6, 0.6, 0.6, 1.0]  // Left face
    ];
    var unpackedColors = [];
    for (var i in colors) {
        var color = colors[i];
        for (var j=0; j < 4; j++) {
            unpackedColors = unpackedColors.concat(color);
        }
    }
    this.gl.bufferData(this.gl.ARRAY_BUFFER, new Float32Array(unpackedColors), this.gl.STATIC_DRAW);
    this.cubeVertexColorBuffer.itemSize = 4;
    this.cubeVertexColorBuffer.numItems = 24;

    this.cubeVertexIndexBuffer = this.gl.createBuffer();
    this.gl.bindBuffer(this.gl.ELEMENT_ARRAY_BUFFER, this.cubeVertexIndexBuffer);
    var cubeVertexIndices = [
        0, 1, 2,      0, 2, 3,    // Front face
        4, 5, 6,      4, 6, 7,    // Back face
        8, 9, 10,     8, 10, 11,  // Top face
        12, 13, 14,   12, 14, 15, // Bottom face
        16, 17, 18,   16, 18, 19, // Right face
        20, 21, 22,   20, 22, 23  // Left face
    ];
    this.gl.bufferData(this.gl.ELEMENT_ARRAY_BUFFER, new Uint16Array(cubeVertexIndices), this.gl.STATIC_DRAW);
    this.cubeVertexIndexBuffer.itemSize = 1;
    this.cubeVertexIndexBuffer.numItems = 36;
};

Pong.RendererCanvasWebGL.prototype.getShader = function(id) {
    var shaderScript = document.getElementById(id);
    if (!shaderScript) {
        return null;
    }

    var str = "";
    var k = shaderScript.firstChild;
    while (k) {
        if (k.nodeType == 3) {
            str += k.textContent;
        }
        k = k.nextSibling;
    }

    var shader;
    if (shaderScript.type == "x-shader/x-fragment") {
        shader = this.gl.createShader(this.gl.FRAGMENT_SHADER);
    } else if (shaderScript.type == "x-shader/x-vertex") {
        shader = this.gl.createShader(this.gl.VERTEX_SHADER);
    } else {
        return null;
    }

    this.gl.shaderSource(shader, str);
    this.gl.compileShader(shader);

    if (!this.gl.getShaderParameter(shader, this.gl.COMPILE_STATUS)) {
        throw this.gl.getShaderInfoLog(shader);
    }

    return shader;
};

Pong.RendererCanvasWebGL.prototype.initShaders = function() {
    var fragmentShader = this.getShader("shader-fs");
    var vertexShader = this.getShader("shader-vs");

    this.shaderProgram = this.gl.createProgram();
    this.gl.attachShader(this.shaderProgram, vertexShader);
    this.gl.attachShader(this.shaderProgram, fragmentShader);
    this.gl.linkProgram(this.shaderProgram);

    if (!this.gl.getProgramParameter(this.shaderProgram, this.gl.LINK_STATUS)) {
        throw "Could not initialise shaders";
    }

    this.gl.useProgram(this.shaderProgram);

    this.shaderProgram.vertexPositionAttribute = this.gl.getAttribLocation(this.shaderProgram, "aVertexPosition");
    this.gl.enableVertexAttribArray(this.shaderProgram.vertexPositionAttribute);

    this.shaderProgram.vertexColorAttribute = this.gl.getAttribLocation(this.shaderProgram, "aVertexColor");
    this.gl.enableVertexAttribArray(this.shaderProgram.vertexColorAttribute);

    this.shaderProgram.pMatrixUniform = this.gl.getUniformLocation(this.shaderProgram, "uPMatrix");
    this.shaderProgram.mvMatrixUniform = this.gl.getUniformLocation(this.shaderProgram, "uMVMatrix");
};

Pong.RendererCanvasWebGL.prototype.mvPushMatrix = function() {
    var copy = mat4.create();
    mat4.set(this.mvMatrix, copy);
    this.mvMatrixStack.push(copy);
};

Pong.RendererCanvasWebGL.prototype.mvPopMatrix = function() {
    if (this.mvMatrixStack.length == 0) {
        throw "Invalid popMatrix!";
    }
    this.mvMatrix = this.mvMatrixStack.pop();
};


Pong.RendererCanvasWebGL.prototype.setMatrixUniforms = function() {
    this.gl.uniformMatrix4fv(this.shaderProgram.pMatrixUniform, false, this.pMatrix);
    this.gl.uniformMatrix4fv(this.shaderProgram.mvMatrixUniform, false, this.mvMatrix);
};


Pong.RendererCanvasWebGL.prototype.degToRad = function(degrees) {
    return degrees * Math.PI / 180;
};

Pong.RendererCanvasWebGL.prototype.initialize = function(canvas) {
    if (!canvas) {
        this.canvas = document.createElement(navigator.isCocoonJS ? "screencanvas": "canvas");
        this.canvas.width = window.innerWidth;
        this.canvas.height = window.innerHeight;
        document.body.appendChild(this.canvas);
    }
    else {
        this.canvas = canvas;
    }

    try {
        this.gl = this.canvas.getContext("experimental-webgl");
    } catch (e) {
    }
    if (!this.gl) {
        throw "Could not initialize the WebGL canvas context.";
    }

    this.mvMatrix = mat4.create();
    this.mvMatrixStack = [];
    this.pMatrix = mat4.create();

    this.initShaders();
    this.initCubeBuffers();

    this.gl.enable(this.gl.DEPTH_TEST);
    this.gl.enable(this.gl.CULL_FACE);
    this.gl.frontFace(this.gl.CW);
};

Pong.RendererCanvasWebGL.prototype.renderRectangle = function(x, y, width, height, color) {
    // Render
    this.mvPushMatrix();
        mat4.scale(this.mvMatrix, [1.0, -1.0, 1.0]);
        mat4.translate(this.mvMatrix, [x, y, 0.0]);
        mat4.scale(this.mvMatrix, [width, height, Math.min(width, height) / 2.0]);

        this.gl.bindBuffer(this.gl.ARRAY_BUFFER, this.cubeVertexPositionBuffer);
        this.gl.vertexAttribPointer(this.shaderProgram.vertexPositionAttribute, this.cubeVertexPositionBuffer.itemSize, this.gl.FLOAT, false, 0, 0);

        this.gl.bindBuffer(this.gl.ARRAY_BUFFER, this.cubeVertexColorBuffer);
        this.gl.vertexAttribPointer(this.shaderProgram.vertexColorAttribute, this.cubeVertexColorBuffer.itemSize, this.gl.FLOAT, false, 0, 0);

        this.gl.bindBuffer(this.gl.ELEMENT_ARRAY_BUFFER, this.cubeVertexIndexBuffer);
        this.setMatrixUniforms();
        this.gl.drawElements(this.gl.TRIANGLES, this.cubeVertexIndexBuffer.numItems, this.gl.UNSIGNED_SHORT, 0);
    this.mvPopMatrix();
};

Pong.RendererCanvasWebGL.prototype.getWidth = function() {
    return this.canvas.width;
};

Pong.RendererCanvasWebGL.prototype.getHeight = function() {
    return this.canvas.height;
};

Pong.RendererCanvasWebGL.prototype.clear = function(color) {
    this.gl.clearColor(0.0, 0.0, 0.0, 1.0);
    this.gl.clear(this.gl.COLOR_BUFFER_BIT | this.gl.DEPTH_BUFFER_BIT);
};

Pong.RendererCanvasWebGL.prototype.beginFrame = function() {
    // Setup the rendering viewport and the camera perspective
    this.gl.viewport(0, 0, this.canvas.width, this.canvas.height);
    mat4.perspective(90, this.canvas.width / this.canvas.height, 0.1, 1000.0, this.pMatrix);

    // Position the camera to be able to see the whole scene.
    mat4.identity(this.mvMatrix);
    mat4.translate(this.mvMatrix, [-this.canvas.width / 2.0, this.canvas.height / 2.0, -Math.min(this.canvas.width, this.canvas.height) / 2.0]);
};