/**
* Author: Iker Jamardo Zugaza
* Company: Ludei (www.ludei.com)
* Disclaimer: 
    Please, note that the purpose of this software is merely educational. Plenty
    of error checking should be added to make it for professional use.
* License: LGPL v3
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
* =====================
* Pong.Rectangle
* =====================
*/
Pong.Rectangle = function() {
};

Pong.Rectangle.prototype = {
    x : 0,
    y : 0,
    width : 0,
    height : 0,

    initialize : function(x, y, width, height) {
        this.x = x || 0;
        this.y = y || 0;
        this.width = width;
        this.height = height;
    },

    collides : function(rectangle) {
        var thisXPlusWidth = this.x + this.width;
        var thisYPlusHeight = this.y + this.height;
        var rectangleXPlusWidth = rectangle.x + rectangle.width;
        var rectangleYPlusHeight = rectangle.y + rectangle.height;
        if (this.x > rectangleXPlusWidth) {
            return false;
        }
        if (this.y > rectangleYPlusHeight) {
            return false;
        }
        if (rectangle.x > thisXPlusWidth) {
            return false;
        }
        if (rectangle.y > thisYPlusHeight) {
            return false;
        }
        return true;
    }
};

/**
* =====================
* Pong.MovingRectangle
* =====================
*/
Pong.MovingRectangle = function() {
};

Pong.extend(Pong.MovingRectangle, Pong.Rectangle);

Pong.MovingRectangle.prototype.lastX = 0;
Pong.MovingRectangle.prototype.lastY = 0;
Pong.MovingRectangle.prototype.dirX = 0;
Pong.MovingRectangle.prototype.dirY = 0;
Pong.MovingRectangle.prototype.speed = 0;

Pong.MovingRectangle.prototype.initialize = function(x, y, width, height, dirX, dirY, speed) {
    Pong.MovingRectangle.superClass.initialize.call(this, x, y, width, height);
    this.lastX = x;
    this.lastY = y;
    this.dirX = dirX || 0;
    this.dirY = dirY || 0;
    this.speed = speed || 0;
};

Pong.MovingRectangle.prototype.update = function(elapsedTimeInSeconds) {
    this.lastX = this.x;
    this.lastY = this.y;
    this.x += this.dirX * this.speed * elapsedTimeInSeconds;
    this.y += this.dirY * this.speed * elapsedTimeInSeconds;
};
