/**
* Author: Iker Jamardo Zugaza
* Company: Ludei (www.ludei.com)
* Disclaimer: 
    Please, note that the purpose of this software is merely educational. Plenty
    of error checking should be added to make it for professional use.
* License: LGPL v3
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
* =====================
* Pong.FPSCounter
* =====================
* Represents a frames per second counter. In fact, it is a timer but that counts the number of frames on
* every update and calculated the average frames that "should" be in the accumulated time in that "second".
*/
Pong.FPSCounter = function() {
};

// The frames per second counter is indeed an extended timer.
Pong.extend(Pong.FPSCounter, Pong.Timer);

Pong.FPSCounter.prototype.fps = 0;
Pong.FPSCounter.prototype.averageFPS = 0;

/**
* The same behavior as in the timer but resets the frames per second counters.
*/
Pong.FPSCounter.prototype.reset = function() {
    Pong.FPSCounter.superClass.reset.call(this);
    this.fps = this.averageFPS = 0;
};

/**
* The same behavior as in the timer but calculates the average frames per second every second.
*/
Pong.FPSCounter.prototype.update = function() {
    Pong.FPSCounter.superClass.update.call(this);
    this.fps++;
    if (this.accumTimeInMillis >= 1000) {
        // Calculate the average fps that "should" have happened depending on the fps count and 
        // the accumulated time. Round the value to only have two decimals ;).
        this.averageFPS = ((1000.0 * this.fps / this.accumTimeInMillis * 100.0) | 0) / 100.0;
        this.reset();
    }
};
