/**
* Author: Iker Jamardo Zugaza
* Company: Ludei (www.ludei.com)
* Disclaimer: 
    Please, note that the purpose of this software is merely educational. Plenty
    of error checking should be added to make it for professional use.
* License: LGPL v3
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
* ======================
* Pong
* ======================
* This is the main namespace and function object in the whole program. Everything that
* is defined will be defined inside it's scope. All the elements are tuned for the purpose
* of this game only, but should be fairly easy to be adapted for a more general purpose game
* engine.
*/
Pong = function() {
};

/**
* Function to allow prototipical inheritance in JavaScript. Derived function will extend from Base function.
* The prototype of the Derived function will point to an intermediate type whose prototype will point to the Base
* function prototype. This ensures that any missing property/function will be search for in the prototype chain.
* @param [function] Derived The function that will represent the derived class.
* @param [function] Base The function that will represent the base class.
*/
Pong.extend = function(Derived, Base) {
     var F = function() {};
     F.prototype = Base.prototype;
     Derived.prototype = new F();
     Derived.prototype.constructor = Derived;
     Derived.superClass = Base.prototype;
};
